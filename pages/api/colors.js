export default function getColorsData(req, res) {
    fetch('https://reqres.in/api/products').then((result) => {
        if (result.ok) {
            return result.json();
        }
        return Promise.reject(result.status);
    }).then(data => {
        res.status(200).json(data.data);
    }).catch((err) => {
        res.status(err);
        console.log(err);
    });
}
