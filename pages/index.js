import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { signIn, signOut, useSession } from 'next-auth/client'
import Link from 'next/link'


export default function Home() {
  const [session, loading] = useSession();

  return (
    <div className={styles.container}>
      <Head>
        <title>Project</title>
        <meta name="description" content="Project - get Color" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1>Project</h1>
       
        {!session && <>
          <p>Login and see the colors</p>
          Not signed in <br />
          <button className={styles.button} style={{backgroundColor: "red"}} onClick={() => signIn()}>Sign in</button>
        </>}
        {session && <>
          Signed in as {session.user.name} <br />
          <button className={styles.button} style={{backgroundColor: "red"}} onClick={() => signOut()}>Sign out</button>
          <Link href="/colors">
            <a className={styles.button}>Colors</a>
          </Link>
        </>}
        
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}
