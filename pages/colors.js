import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'

import styles from '../styles/Home.module.css'
import stylesCard from '../styles/card.module.css'

import { useState, useEffect } from 'react'
import { useSession, signIn } from 'next-auth/client'

export default function Home({colorsData}) {
  const [ session, loading ] = useSession()

  

  // When rendering client side don't display anything until loading is complete
  if (typeof window !== 'undefined' && loading) return null

  // If no session exists, display access denied message
  if (!session) { return  (<><p>You must be logged in to visit the page</p> <button onClick={() => signIn()}>Sign in</button> </>) }

  console.log(colorsData);
  // If session exists, display content
  return (
    <div className={styles.container}>
      <Head>
        <title>Color</title>
        <meta name="description" content="Project - get Color" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
         <h1>Color</h1>
         <p>look at the colors</p>
            <ul className={stylesCard.cards}>
            {colorsData!== undefined ? colorsData.map((colorData) => (
              <li className={stylesCard.cards__item} key={colorData.id} style={{backgroundColor: colorData.color}}>
                <p className={stylesCard.cards__title}>{colorData.name}</p>
              </li>
            )) : <></>}
            </ul>
          <Link href="/">
            <a className={styles.backToHome}>← Back to home</a>
          </Link>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}


export async function getServerSideProps() {
  const res = await fetch(`https://reqres.in/api/products`)
  const data = await res.json()
  const colorsData = data.data;
  return { props: { colorsData }  }
}
